package io.shantech.SpringVideo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping("/home")
    public String viewHomePage(){
        return "home";
    }

}

package io.shantech.SpringVideo.Service;

import io.shantech.SpringVideo.Entity.Video;
import io.shantech.SpringVideo.Exception.VideoAlreadyExistsException;
import io.shantech.SpringVideo.Repository.VideoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@AllArgsConstructor
public class VideoServiceImpl implements VideoService {
        private VideoRepository repo;

        @Override
        public Video getVideo(String name) {
            if(!repo.existsByName(name)){
                throw new VideoAlreadyExistsException();
            }
            return repo.findByName(name);
        }

        @Override
        public List<String> getAllVideoNames() {
            return repo.getAllEntryNames();
        }

        @Override
        public void saveVideo(MultipartFile file, String name) throws IOException {
           // if(repo.existsByName(name)){
                //throw new VideoAlreadyExistsException();
           // }
            //byte[] videoData = file.getBytes();
            Video newVid = new Video(name,file.getBytes()) ;
            repo.save(newVid);
        }
    }

